from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS
import os

app = Flask(__name__)
CORS(app)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'contrato.sqlite')
db = SQLAlchemy(app)
ma = Marshmallow(app)



class Contrato(db.Model):
    idContrato = db.Column(db.Integer, primary_key=True)
    descricao = db.Column(db.String(80), unique=False)
    dataAssinatura = db.Column(db.String(80), unique=False)
    dataValidade = db.Column(db.String(80), unique=False)
    statusContrato = db.Column(db.String(80), unique=False)
    funcionarioNome = db.Column(db.String(80), unique=False)


    def __init__(self, idContrato, descricao,dataAssinatura, dataValidade,statusContrato,funcionarioNome):
        self.idContrato = idContrato
        self.descricao = descricao
        self.dataAssinatura = dataAssinatura
        self.dataValidade = dataValidade
        self.statusContrato = statusContrato
        self.funcionarioNome = funcionarioNome



class ContratoSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('idContrato', 'descricao','dataAssinatura', 'dataValidade', 'statusContrato','funcionarioNome')


contrato_schema = ContratoSchema()
contratos_schema = ContratoSchema(many=True)


# endpoint to create new contrato
@app.route("/contratoAdd",methods=["POST"])
def add_contrato():
    descricao = request.json['descricao']
    dataAssinatura = request.json['dataAssinatura']
    dataValidade = request.json['dataValidade']
    statusContrato = request.json['statusContrato']
    funcionarioNome = request.json['funcionarioNome']

    new_contrato = Contrato(descricao, dataAssinatura, dataValidade, statusContrato,funcionarioNome)

    db.session.add(new_contrato)
    db.session.commit()

    return jsonify(new_contrato)

@app.route("/contratos", methods=["GET"])
def get_contratos():
    todos_contratos = Contrato.query.all()
    result = contratos_schema.dump(todos_contratos)
    return jsonify(result.data)

@app.route("/contrato/<idContrato>", methods=["GET"])
def contrato_detalhes(idContrato):
    contrato = Contrato.query.get(idContrato)
    return contrato_schema.jsonify(contrato)


@app.route("/contratoPorFuncionario/<idFuncionario>", methods=["GET"])
def contrato_detalhesPorIdFuncionario(idFuncionario):
    contrato = Contrato.query.get(idFuncionario)

    return contrato_schema.jsonify(contrato)

@app.route("/contratoEdit/<idContrato>", methods=["PUT"])
def funcionario_update(idContrato):
    contrato = Contrato.query.get(idContrato)
    idContrato = request.json['idContrato']
    descricao = request.json['descricao']
    dataAssinatura = request.json['dataAssinatura']
    dataValidade = request.json['dataValidade']
    statusContrato = request.json['statusContrato']
    funcionarioNome = request.json['funcionarioNome']



    contrato.idContrato = idContrato
    contrato.descricao = descricao
    contrato.dataAssinatura = dataAssinatura
    contrato.dataValidade = dataValidade
    contrato.statusContrato = statusContrato
    contrato.funcionarioNome = funcionarioNome



    db.session.commit()
    return contrato_schema.jsonify(contrato)

@app.route("/contratoDelete/<idContrato>", methods=["DELETE"])
def contrato_delete(idContrato):
    contrato = Contrato.query.get(idContrato)
    db.session.delete(contrato)
    db.session.commit()

    return contrato_schema.jsonify(contrato)

if __name__ == '__main__':
    app.run(debug=True,port=5001)

